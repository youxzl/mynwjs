/**
 * Created by gaocheng on 2016/11/7.
 */
var fs = require("fs");
var http = require("http");
var url = require("url");
var gui = require('nw.gui');
var win = gui.Window.get();
var path = require("path");
var exec = require('child_process').exec;

var cloneObj = function (obj) {
    var str, newobj = obj.constructor === Array ? [] : {};
    if (typeof obj !== 'object') {
        return;
    } else if (window.JSON) {
        str = JSON.stringify(obj), //系列化对象
            newobj = JSON.parse(str); //还原
    } else {
        for (var i in obj) {
            newobj[i] = typeof obj[i] === 'object' ?
                cloneObj(obj[i]) : obj[i];
        }
    }
    return newobj;
};

var _i = {//核心的结构性内容
    _vars: {},//
    thisurl: "",
    thisurlinfo:{},
    uget:{},
    mainpath:"",//运行主目录
    execpath:"",//nw.exe运行目录
    rootpath:"",//
    datapath:"",
    init: function () {//初始化参数
        this.thisurl = document.URL;
        this.execpath=path.dirname(process.execPath);
        this.mainpath=this.execpath.substring(0,this.execpath.lastIndexOf(path.sep));
        //rootPath
        var rootArray = window.document.location.href.split("/");
        this.rootpath = rootArray[0] + "//" + rootArray[2];
        //数据存储目录
        this.datapath = gui.App.dataPath;

        this.urlinit();
    },
    v: function (path, vars) {//加载html模板
        var tmpstr = fs.readFileSync(path);
        if (arguments.length == 2) {
            this._vars = cloneObj(arguments[1]);
        }
        document.write(tmpstr);
    },
    o: function (varele) {//模板中输出内容
        document.write(this._vars[varele]);
    },
    urlinit: function () {//url初始化
        this.urlinfo=url.parse(this.thisurl,true);
        this.uget=this.urlinfo.query;
    },
    a:function(_p,data){//建立适用于当前程序的界面跳转链接
        var tmpstr="start.html?_p="+_p;
        for(var k in data)
        {
            tmpstr+="&"+k+"="+data[k];
        }
        return tmpstr;
    },
    newwin:function (_p,data,w,h) {//打开新窗口
        w=w?w:800;
        h=h?h:600;

        return gui.Window.open(this.a(_p,data), {
                position: 'center',
                width: w,
                height: h,
                focus: true
            });
        },
    ititle:function (t) {
        $("title").html(t);
    }


};
var _comm={//常用的方法
    icmd:function(cmdstr){//调用系统命令行工具运行命令
        exec(cmdstr,function(){});
    },
    watch:function (ary,callbak) {//监视文件夹内容变动
        var paths = ary;
        for (var p in paths) {
            fs.watch(paths[p], function(event,filename){
                callbak(event,filename);
            });
        }
    }
};
_i.init();